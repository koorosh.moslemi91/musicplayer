import sys , os
from os.path import expanduser
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtMultimedia import *
from PyQt5.QtCore import *
from UI import PlayerUI
from util import *
import random
from Library import LibraryForm
from ManagePlaylist import ManagePlaylistForm



class PlayerWindow(QMainWindow):
    def __init__(self,):
        super().__init__()

        PlayerUI.setupUi(self)
        self.status = self.statusBar()
        self.status.showMessage('No Media')
        pixmap = QtGui.QPixmap("Assets/Default.jpg")
        self.imageLabel.setPixmap(pixmap)
        dbConnect()

        self.currentPlaylist = QMediaPlaylist()
        self.player = QMediaPlayer()
        self.userAction = -1			#1- playing 2-paused
        self.setVolume()
        self.playingIndex = -1
        self.mediaUrls = []


        self.actionOpen.triggered.connect(self.openFile)
        self.actionChoose_from_Library.triggered.connect(lambda:self.openFile(True))
        self.actionOpenLibrary.triggered.connect(self.showLibrary)
        self.actionManagePlaylist.triggered.connect(self.showPlaylist)
        


        self.playButton.clicked.connect(self.playPolicy)
        self.soundDial.valueChanged.connect(self.setVolume)
        self.player.mediaStatusChanged.connect(self.mediaStatusChanged)
        self.player.positionChanged.connect(self.mediaPositionChanged)
        self.rightNumber.clicked.connect(self.showRemainedTime)
        self.musicSlider.sliderMoved.connect(self.sliderPosition)
        self.speedDial.valueChanged.connect(self.setSpeed)
        self.nextButton.clicked.connect(self.nextItemPlaylist)
        self.prevButton.clicked.connect(self.prevItemPlaylist)
        self.imageLabel.clicked.connect(self.mediaDetail)
        self.shuffleButton.clicked.connect(self.shufflePlaylist)
        self.removeFromQueueButton.clicked.connect(self.removeMediaFromQueue)
        self.queueList.doubleClicked.connect(self.changePlayingMedia)
        self.upButton.clicked.connect(self.mediaUpwardInQueue)
        self.downButton.clicked.connect(self.mediaDownwardInQueue)
        self.repeatButton.clicked.connect(self.repeatQueue)
        #self.queueList.itemSelectionChanged.connect(self.disableItemClick)
        


    def openFile(self,libraryCall = False):
        if libraryCall:
            base = os.path.dirname(os.path.abspath(__file__))
            fileChoosen = QFileDialog.getOpenFileNames(self,'Open Music From Library', base + "\\Assets\\Library\\", 'Audio (*.mp3 *.ogg *.wav)')
        else:
            fileChoosen = QFileDialog.getOpenFileUrls(self,'Open Music File', expanduser('~'),'Audio (*.mp3 *.ogg *.wav)','*.mp3 *.ogg *.wav')
        for mediaQUrl in fileChoosen[0]:
            if isinstance(mediaQUrl,str):
                mediaQUrl = QUrl("file:///" + mediaQUrl)
            self.mediaUrls.append(mediaQUrl)
            self.currentPlaylist.addMedia(QMediaContent(mediaQUrl))
            _,filename = os.path.split(QUrl(mediaQUrl).url())
            self.queueList.addItem(QListWidgetItem(filename))
        if self.playingIndex == -1:
            self.playingIndex = 0
        self.queueList.setCurrentItem(self.queueList.item(self.playingIndex))

    def openPlaylist(self,filenames):
        base = os.path.dirname(os.path.abspath(__file__))
        for filename in filenames:
            mediaQUrl = QUrl(f"file:///{base}/Assets/Library/{filename}")
            self.mediaUrls.append(mediaQUrl)
            self.currentPlaylist.addMedia(QMediaContent(mediaQUrl))
            filename = os.path.splitext(os.path.basename(filename))[0]
            print("test",filename)
            self.queueList.addItem(QListWidgetItem(filename))
        if self.playingIndex == -1:
            self.playingIndex = 0
        self.queueList.setCurrentItem(self.queueList.item(self.playingIndex))

    def showLibrary(self):
        self.window = LibraryForm()
        self.window.show()


    def showPlaylist(self):
        self.window2 = ManagePlaylistForm(self)
        self.window2.show()


    def playPolicy(self):
        if self.player.state() == QMediaPlayer.PlayingState:
            self.pauseHandler()
            self.playButton.setText("Play")
        else:
            self.playHandler()
            self.playButton.setText("Pause")
    
    def playHandler(self):
    		self.userAction = 1
    		if self.player.state() == QMediaPlayer.StoppedState :
    			if self.player.mediaStatus() == QMediaPlayer.NoMedia:
    				#self.player.setMedia(QMediaContent(QUrl.fromLocalFile(self.currentFile)))
    				print(self.currentPlaylist.mediaCount())
    				if self.currentPlaylist.mediaCount() == 0:
    					self.openFile()
    				if self.currentPlaylist.mediaCount() != 0:
    					self.player.setPlaylist(self.currentPlaylist)
    			elif self.player.mediaStatus() == QMediaPlayer.LoadedMedia:
    				self.player.play()
    			elif self.player.mediaStatus() == QMediaPlayer.BufferedMedia:
    				self.player.play()
    		elif self.player.state() == QMediaPlayer.PlayingState:
    			return
    		elif self.player.state() == QMediaPlayer.PausedState:
    			self.player.play()



    def pauseHandler(self):
    	self.userAction = 2
    	self.player.pause()

    def setVolume(self):
        vol = self.soundDial.value()
        self.player.setVolume(vol)
        self.volumeLabel.setText(f"Volume: {vol}")


    def mediaStatusChanged(self,status):
        if status == QMediaPlayer.EndOfMedia:
            self.nextItemPlaylist()

        self.rightNumber.setText(hhmmss(self.player.duration()))

        self.musicSlider.setValue(0)
        self.musicSlider.setMinimum(0)
        self.musicSlider.setMaximum(self.player.duration())

        _,filename = os.path.split(QUrl(self.mediaUrls[self.playingIndex]).url())
        self.status.showMessage(f"Playing {filename}")

        self.player.play()
        

    def mediaPositionChanged(self):
        self.leftNumber.setText(hhmmss(self.player.position()))
        self.musicSlider.setValue(self.player.position())
        if '-' in self.rightNumber.text():
            self.rightNumber.setText("-" + hhmmss(self.player.duration() - self.player.position()))

    def showRemainedTime(self):
        if '-' in self.rightNumber.text():
            self.rightNumber.setText(hhmmss(self.player.duration()))
        else:
            self.rightNumber.setText("-" + hhmmss(self.player.duration() - self.player.position()))

    def sliderPosition(self, position):
        if self.player.isSeekable():
            self.player.setPosition(position)

    def setSpeed(self):     
        currentSpeed = self.speedDial.value() / 50
        self.player.setPlaybackRate(currentSpeed)
        self.speedLabel.setText(f"Speed Rate: {currentSpeed:.1f}x")

    def prevItemPlaylist(self):
        if self.playingIndex > 0:
            self.player.playlist().previous()
            self.playingIndex -= 1
            self.queueList.setCurrentItem(self.queueList.item(self.playingIndex))

    def nextItemPlaylist(self):
        if self.playingIndex < self.currentPlaylist.mediaCount():
            self.player.playlist().next()
            self.playingIndex += 1
            self.queueList.setCurrentItem(self.queueList.item(self.playingIndex))

    def mediaDetail(self):
        if self.player.mediaStatus() != QMediaPlayer.NoMedia:
            mediaTitle = self.player.metaData(QMediaMetaData.Title)
            albumTitle = self.player.metaData(QMediaMetaData.AlbumTitle)
            albumArtist = self.player.metaData(QMediaMetaData.AlbumArtist)
            genreList = self.player.metaData(QMediaMetaData.Genre)
            if genreList is not None:
                genre = ",".join(self.player.metaData(QMediaMetaData.Genre))
            else:
                genre = "None"
            self.metaInfoMessage(mediaTitle,albumTitle,albumArtist,genre)

    def metaInfoMessage(self,mediaTitle , albumTitle , albumArtist , genre):
        msg = QMessageBox()
        msg.setWindowTitle("Meta Data")
        msg.setText(f"Title: {mediaTitle}\nAlbum Title: {albumTitle}\nAlbum Artist: {albumArtist}\nGenre: {genre}")
        msg.setIcon(QMessageBox.Information)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.setDefaultButton(QMessageBox.Ok)
        return msg.exec_()

    def disableItemClick(self):
        self.queueList.setCurrentItem(self.queueList.item(self.playingIndex))

    def shufflePlaylist(self):
        #self.currentPlaylist.shuffle()
        random.shuffle(self.mediaUrls)
        self.currentPlaylist.clear()
        self.queueList.clear()
        for mediaQUrl in self.mediaUrls:
            self.currentPlaylist.addMedia(QMediaContent(mediaQUrl))
            _,filename = os.path.split(QUrl(mediaQUrl).url())
            self.queueList.addItem(QListWidgetItem(filename))
        self.playingIndex = 0
        self.queueList.setCurrentItem(self.queueList.item(self.playingIndex))
        self.player.setPlaylist(self.currentPlaylist)
        self.playButton.setText("Pause")

    def removeMediaFromQueue(self):
        selectedIndexes = [x.row() for x in self.queueList.selectedIndexes()]
        mediaQUrl = self.mediaUrls[self.playingIndex]
        self.mediaUrls.remove(self.mediaUrls[selectedIndexes[0]])
        self.playingIndex = self.mediaUrls.index(mediaQUrl)
        self.currentPlaylist.removeMedia(selectedIndexes[0])
        self.queueList.takeItem(selectedIndexes[0])

    def changePlayingMedia(self):
        selectedIndexes = [x.row() for x in self.queueList.selectedIndexes()]
        self.currentPlaylist.setCurrentIndex(selectedIndexes[0])
        self.playingIndex = selectedIndexes[0]

    def mediaUpwardInQueue(self):
        selectedIndexes = [x.row() for x in self.queueList.selectedIndexes()]
        selectedIndex = selectedIndexes[0]

        if selectedIndex > 0  and selectedIndex > self.playingIndex:
            #current playlist modifications
            selectedMedia = self.currentPlaylist.media(selectedIndex)
            self.currentPlaylist.insertMedia(selectedIndex-1,selectedMedia)
            self.currentPlaylist.removeMedia(selectedIndex+1)

            #media urls modification
            self.mediaUrls[selectedIndex-1] , self.mediaUrls[selectedIndex] = self.mediaUrls[selectedIndex] , self.mediaUrls[selectedIndex-1]

            #queue list widget modifiction
            selectedItem = self.queueList.takeItem(selectedIndex)
            self.queueList.insertItem(selectedIndex - 1 , selectedItem)

    def mediaDownwardInQueue(self):
        selectedIndexes = [x.row() for x in self.queueList.selectedIndexes()]
        selectedIndex = selectedIndexes[0]

        if selectedIndex < self.currentPlaylist.mediaCount()  and selectedIndex > self.playingIndex:
            #current playlist modifications
            selectedMedia = self.currentPlaylist.media(selectedIndex)
            self.currentPlaylist.insertMedia(selectedIndex+2,selectedMedia)
            self.currentPlaylist.removeMedia(selectedIndex)

            #media urls modification
            self.mediaUrls[selectedIndex+1] , self.mediaUrls[selectedIndex] = self.mediaUrls[selectedIndex] , self.mediaUrls[selectedIndex+1]

            #queue list widget modifiction
            selectedItem = self.queueList.takeItem(selectedIndex)
            self.queueList.insertItem(selectedIndex + 1 , selectedItem)

    def repeatQueue(self):
        self.player.setPlaylist(self.currentPlaylist)
        self.playingIndex = 0
        self.queueList.setCurrentItem(self.queueList.item(self.playingIndex))






if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ui = PlayerWindow()
    ui.show()
    sys.exit(app.exec_())

