from PyQt5 import QtCore, QtGui, QtWidgets
from UI import ManagePlaylistUI
from PyQt5.QtGui import QStandardItemModel,QStandardItem
from PyQt5.QtSql import QSqlQuery,QSqlDatabase
from util import hhmmss,dbConnect,criticalMessage
from CreatePlaylist import CreatePlaylistForm

loadedPlaylistsId = []

class ManagePlaylistForm(QtWidgets.QWidget):
    def __init__(self,parentWindow):
        QtWidgets.QWidget.__init__(self)
        ManagePlaylistUI.setupUi(self)


        self.parentWindow = parentWindow
        #temporary line
        #dbConnect()

        self.loadPlaylists()

        self.removeButton.clicked.connect(self.removePlaylist)
        self.editButton.clicked.connect(self.showEditPlaylistForm)
        self.createButton.clicked.connect(lambda :self.showEditPlaylistForm(True))
        self.playButton.clicked.connect(self.playPlaylist)

    def playPlaylist(self):
        selectedIndexes = [x.row() for x in self.playlistsTreeView.selectedIndexes()]
        if selectedIndexes != []:
            selectedPlaylistId = loadedPlaylistsId[selectedIndexes[0]]
            query = QSqlQuery()
            query.exec_(f"select path from PlaylistSong as ps inner join Song as S on ps.songId = S.id where ps.playlistId = {selectedPlaylistId}")
            
            song_names = []
            while(query.next()):
                song_names.append(query.value(0))

            self.parentWindow.openPlaylist(song_names)
            self.close()
        else:
            criticalMessage("You have not selected a playlist")

    def loadPlaylists(self):

        loadedPlaylistsId.clear()

        model = QStandardItemModel(0,2)
        rootNode = model.invisibleRootItem()
        model.setHorizontalHeaderLabels(["Playlist Name" , "Playlist Duration"])

        query = QSqlQuery()
        query.exec_("select p.id,p.name,sum(S.duration) as durationSum from PlaylistSong as ps inner join Playlist as P on ps.playlistId = P.id inner join Song as S on ps.songId = S.id group by P.name")
        while(query.next()):
            id = query.value(0)
            name = query.value(1)
            durationSum = hhmmss(query.value(2))

            loadedPlaylistsId.append(id)
            rootNode.appendRow([QStandardItem(name) , QStandardItem(durationSum)])

        self.playlistsTreeView.reset()
        self.playlistsTreeView.setModel(model)

    def removePlaylist(self):
        #delete playlists songs
        #delete playlist itself

        selectedIndexes = [x.row() for x in self.playlistsTreeView.selectedIndexes()]
        if selectedIndexes != []:
            selectedPlaylistId = loadedPlaylistsId[selectedIndexes[0]]

            query = QSqlQuery()
            query.exec_(f"delete from PlaylistSong where playlistId = {selectedPlaylistId}")

            query = QSqlQuery()
            query.exec_(f"delete from Playlist where id = {selectedPlaylistId}")

            self.loadPlaylists()
        else:
            criticalMessage("You have not selected a playlist")


    def showEditPlaylistForm(self,createPlaylist = False):

        if createPlaylist:
            self.form = CreatePlaylistForm(parentWindow=self)
            self.form.show()

        else:

            selectedIndexes = [x.row() for x in self.playlistsTreeView.selectedIndexes()]
            if selectedIndexes != []:
                selectedPlaylistId = loadedPlaylistsId[selectedIndexes[0]] 
                self.form = CreatePlaylistForm(selectedPlaylistId,self)
                self.form.show()
            
            
            else:
                criticalMessage("You should choose a playlist first")




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ui = ManagePlaylistForm()
    ui.show()
    sys.exit(app.exec_())

