from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtSql import QSqlQuery,QSqlDatabase

def dbConnect():
    db = QSqlDatabase.addDatabase("QSQLITE")
    db.setDatabaseName("Music.db")
    if not db.open():
        QMessageBox.critical(None, "Cannot open database","Unable to establish a database connection.", QMessageBox.Cancel)
        return
    enableForeignKey()

def enableForeignKey():
    query = QSqlQuery()
    query.exec_("PRAGMA foreign_keys=ON")

def criticalMessage(message):
    msg = QMessageBox()
    msg.setWindowTitle("Error")
    msg.setText(message)
    msg.setIcon(QMessageBox.Critical)
    msg.setStandardButtons(QMessageBox.Ok)
    msg.setDefaultButton(QMessageBox.Ok)
    return msg.exec_()

def hhmmss(ms):
    h, r = divmod(ms, 3600000)
    m, r = divmod(r, 60000)
    s, _ = divmod(r, 1000)
    return ("%d:%02d:%02d" % (h,m,s)) if h else ("%d:%02d" % (m,s))
