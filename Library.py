from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QStandardItemModel,QStandardItem
from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtSql import QSqlQuery,QSqlDatabase
from PyQt5.QtMultimedia import *
from UI import LibraryUI
from util import *
import os
from os.path import expanduser
from shutil import copy2
from tinytag import TinyTag
import collections


all_songs = []


class LibraryForm(QtWidgets.QWidget):

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        LibraryUI.setupUi(self)

        #Temporary Line - better to push dbconnect in MusicPlayer.py as the startup file
        #dbConnect()

        self.loadSongsFromDb()
        
        #self.testTree()
        self.addButton.clicked.connect(self.addSong)
        self.removeButton.clicked.connect(self.removeSong)
        self.gbNoneButton.clicked.connect(self.renderAllSongsTreeView)
        self.gbArtistButton.clicked.connect(self.groupSongsByArtist)
        self.gbAlbumButton.clicked.connect(self.groupSongsByAlbum)
        self.gbGenreButton.clicked.connect(self.groupSongsByGenre)

    def addSong(self):
        fileChoosen = QFileDialog.getOpenFileUrls(self,'Open Music File', expanduser('~'),'Audio (*.mp3 *.ogg *.wav)','*.mp3 *.ogg *.wav')

        error = self.songFileNameConflict(fileChoosen)
        if(len(error) > 0):
            criticalMessage("Please rename these selected files if they are different from those in your library:\n" + "\n".join(error))
            return

        for mediaQUrl in fileChoosen[0]:
            path = QUrl(mediaQUrl).toLocalFile()
            copy2(path,"Assets/Library")
            _, filename = os.path.split(path)
            self.insertSongDB(filename,self.getSongMetaDataList(path))
        
        self.loadSongsFromDb()

    def removeSong(self):

        selectedIndexes = [x.row() for x in self.libraryView.selectedIndexes()]
        if selectedIndexes != []:
            row = all_songs[selectedIndexes[0]]
            id = row[0]
            all_songs.remove(row)

            query = QSqlQuery()
            query.prepare("delete from Song where id = :id")
            query.bindValue(":id",id)
            if query.exec_() == False:
                criticalMessage("This song is already avaible in a playlist")
                return

            path = f"Assets/Library/{row[1]}"
            if os.path.exists(path):
                os.remove(path)

            self.renderAllSongsTreeView()
        else:
            criticalMessage("You have not selected a song")

    def getSongMetaDataList(self,path):
        tag = TinyTag.get(path)
        print(tag.genre)
        return [str(tag.albumartist),str(tag.album),str(tag.genre),str(int(tag.duration*1000))]
    
    def insertSongDB(self,path,meta):
        query = QSqlQuery()
        query.prepare("insert into Song(path,artistName,albumName,genre,duration) values (:path,:artistName,:albumName,:genre,:duration)")
        print(meta)
        query.bindValue(":path" , path)
        query.bindValue(":artistName" , meta[0])
        query.bindValue(":albumName" , meta[1])
        query.bindValue(":genre" , meta[2])
        query.bindValue(":duration" , meta[3])
        if query.exec_() == False:
            return False
        return True

    def songFileNameConflict(self,fileChoosen):
        error = []
        for mediaQUrl in fileChoosen[0]:
            path = QUrl(mediaQUrl).toLocalFile()
            _, filename = os.path.split(path)
            if os.path.isfile(f"Assets/Library/{filename}"):
                error.append(filename)

        return error

    def loadSongsFromDb(self):
        query = QSqlQuery()
        all_songs.clear()
        query.exec_("select * from Song")

        while query.next():
            id = query.value(0)
            path = query.value(1)
            artistName = query.value(2)
            albumName = query.value(3)
            genre = query.value(4)
            duration = query.value(5)
            all_songs.append([id, path, artistName, albumName, genre, duration])

        self.renderAllSongsTreeView()

    def renderAllSongsTreeView(self):
        self.removeButton.setEnabled(True)
        self.addButton.setEnabled(True)
        model = QStandardItemModel(0,5)
        rootNode = model.invisibleRootItem()
        model.setHorizontalHeaderLabels(["Path" , "Artist Name","Album Name" , "Genre" , "Duration"])

        for song in all_songs:
            rootNode.appendRow([QStandardItem(hhmmss(int(item))) if str(item).isnumeric() else QStandardItem(item) for item in song[1:]])

        self.libraryView.reset()
        self.libraryView.setModel(model)

    def groupSongsByArtist(self):
        #path - album - genre - duration
        self.removeButton.setEnabled(False)
        self.addButton.setEnabled(False)
        data = collections.defaultdict(list)
        for song in all_songs:
            key = song[2]
            song = (song[1],song[3],song[4],hhmmss(int(song[5])))
            data[key].append(song)
        
        model = QStandardItemModel(0,4)
        model.setHorizontalHeaderLabels(["Path","Album Name" , "Genre" , "Duration"])
        rootNode = model.invisibleRootItem()

        for gpName,rows in data.items():
            gpRoot = QStandardItem(gpName)
            for row in rows:
                gpRoot.appendRow([QStandardItem(item) for item in row])
            rootNode.appendRow(gpRoot)

        self.libraryView.reset()
        self.libraryView.setModel(model)

    def groupSongsByAlbum(self):
        #path - artist - genre - duration
        self.removeButton.setEnabled(False)
        self.addButton.setEnabled(False)
        data = collections.defaultdict(list)
        for song in all_songs:
            key = song[3]
            song = (song[1],song[2],song[4],hhmmss(int(song[5])))
            data[key].append(song)
        
        model = QStandardItemModel(0,4)
        model.setHorizontalHeaderLabels(["Path","Artist Name" , "Genre" , "Duration"])
        rootNode = model.invisibleRootItem()

        for gpName,rows in data.items():
            gpRoot = QStandardItem(gpName)
            for row in rows:
                gpRoot.appendRow([QStandardItem(item) for item in row])
            rootNode.appendRow(gpRoot)

        self.libraryView.reset()
        self.libraryView.setModel(model)

    def groupSongsByGenre(self):
        #path - artist - album - duration
        self.removeButton.setEnabled(False)
        self.addButton.setEnabled(False)
        data = collections.defaultdict(list)
        for song in all_songs:
            key = song[4]
            song = (song[1],song[2],song[3],hhmmss(int(song[5])))
            data[key].append(song)
        
        model = QStandardItemModel(0,4)
        model.setHorizontalHeaderLabels(["Path","Artist Name" , "Album Name" , "Duration"])
        rootNode = model.invisibleRootItem()

        for gpName,rows in data.items():
            gpRoot = QStandardItem(gpName)
            for row in rows:
                gpRoot.appendRow([QStandardItem(item) for item in row])
            rootNode.appendRow(gpRoot)

        self.libraryView.reset()
        self.libraryView.setModel(model)

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ui = LibraryForm()
    ui.show()
    sys.exit(app.exec_())
