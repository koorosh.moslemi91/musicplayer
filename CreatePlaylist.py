from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QTreeWidgetItem
from PyQt5.QtSql import QSqlQuery,QSqlDatabase
from PyQt5.Qt import Qt
from UI import CreatePlaylistUI
from util import hhmmss,dbConnect,criticalMessage
from PyQt5.QtWidgets import QMessageBox

all_songs = []
prev_checked_songs = []

class CreatePlaylistForm(QtWidgets.QWidget):

    def __init__(self,playlistId = -1,parentWindow = None):
        QtWidgets.QWidget.__init__(self)
        CreatePlaylistUI.setupUi(self)

        #temporary line
        #dbConnect()


        self.playlistId = playlistId
        self.parentWindow = parentWindow


        if self.playlistId == -1:#Create Playlist
            self.loadSongs()
            self.confirmButton.clicked.connect(self.createPlaylist)
        else:#Remove Playlist
            self.loadSongs()
            self.getPlaylistPrevName()
            self.confirmButton.setText("Edit Playlist")
            self.setWindowTitle("Edit Playlist")
            self.confirmButton.clicked.connect(self.editPlaylist)

    def getPlaylistSongIds(self):
        ids = []
        query = QSqlQuery()
        query.exec_(f"select songId from PlaylistSong where playlistId = {self.playlistId}")

        while query.next():
            ids.append(query.value(0))

        return ids

    def getPlaylistPrevName(self):
        query = QSqlQuery()
        query.exec_(f"select name from Playlist where id = {self.playlistId}")
        query.next()
        self.playlistsPrevName = query.value(0)
        self.playListNameLineEdit.setText(self.playlistsPrevName)

    def loadSongs(self):
        query = QSqlQuery()
        all_songs.clear()
        query.exec_("select * from Song")


        self.songsTreeView.setColumnCount(5)
        self.songsTreeView.setHeaderLabels(["Filename","Artist Name" ,"Album Name" , "Genre" , "Duration"])

        if self.playlistId != -1:
            self.playlistSongIds = self.getPlaylistSongIds()

        while query.next():
            id = query.value(0)
            path = query.value(1)
            artistName = query.value(2)
            albumName = query.value(3)
            genre = query.value(4)
            duration = query.value(5)
            all_songs.append([id, path, artistName, albumName, genre, duration])

            if self.playlistId == -1:
                self.createTreeWidgetItem([path, artistName, albumName, genre, hhmmss(int(duration))],Qt.Unchecked)
            else:
                if id in self.playlistSongIds:
                    self.createTreeWidgetItem([path, artistName, albumName, genre, hhmmss(int(duration))],Qt.Checked)
                else:
                    self.createTreeWidgetItem([path, artistName, albumName, genre, hhmmss(int(duration))],Qt.Unchecked)

        self.songsTreeView.show()

    def createTreeWidgetItem(self,cols,checked):
        item = QTreeWidgetItem(cols)
        item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
        item.setCheckState(0,checked)
        self.songsTreeView.addTopLevelItem(item)

    def editPlaylist(self):###
        playlistName = self.playListNameLineEdit.text()

        if playlistName == "":
            criticalMessage("Name of your playlist cannot be empty")
            return

        song_ids = self.getCheckedSongs()
        if len(song_ids) == 0:
            criticalMessage("Your playlist cannot be empty")
            return

        if set(self.playlistSongIds) == set(song_ids) and playlistName == self.playlistsPrevName:
            criticalMessage("You made no change to your playlist!")
            return

        #update playlist name if neccessary
        #if selected songs were different:
        #delete all previous rows for a specific playlist id
        #add new rows to playlist id

        if playlistName != self.playlistsPrevName:
            query = QSqlQuery()
            query.prepare("update Playlist set name = :newPlaylistName where id = :playlistId")
            query.bindValue(":newPlaylistName",playlistName)
            query.bindValue(":playlistId",self.playlistId)
            query.exec_()

        if set(self.playlistSongIds) != set(song_ids):
            query = QSqlQuery()
            query.exec_(f"delete from PlaylistSong where playlistId = {self.playlistId}")

            values = ",".join([f"({id},{self.playlistId})" for id in song_ids])
            query = QSqlQuery()
            query.exec_("insert into PlaylistSong(songId, playlistId) values " + values)

        self.editSuccessMessage()

    def createPlaylist(self):#####

        playlistName = self.playListNameLineEdit.text()

        if playlistName == "":
            criticalMessage("Name of your playlist cannot be empty")
            return

        song_ids = self.getCheckedSongs()
        if len(song_ids) == 0:
            criticalMessage("Your playlist cannot be empty")
            return

        #check for repeterive playlist name
        #insert playlist and get id
        #insert songs for a specific playlist id

        query = QSqlQuery()
        query.prepare("select count(*) from Playlist where name = :playlistName")
        query.bindValue(":playlistName",playlistName)
        query.exec_()
        query.next()
        if query.value(0) > 0:
            criticalMessage("This playlist name has already been used")
            return


        query = QSqlQuery()
        query.prepare("insert into Playlist(name) values(:playlistName)")
        query.bindValue(":playlistName",playlistName)
        query.exec_()
        playlist_id = query.lastInsertId()

        values = ",".join([f"({id},{playlist_id})" for id in song_ids])
        query = QSqlQuery()
        query.exec_("insert into PlaylistSong(songId, playlistId) values " + values)

        self.close()


    def getCheckedSongs(self):
        root = self.songsTreeView.invisibleRootItem()

        checked_ids = []

        for i in range(root.childCount()):
            item = root.child(i)

            if item.checkState(0) == Qt.Checked:
                checked_ids.append(all_songs[i][0])

        return checked_ids

    def closeEvent(self,event):
        if(self.parentWindow != None):
            self.parentWindow.loadPlaylists()

    def editSuccessMessage(self):
        msg = QMessageBox()
        msg.setWindowTitle("Playlist Edited")
        msg.setText(f"Your playlist has been updated successfully!")
        msg.setIcon(QMessageBox.Information)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.setDefaultButton(QMessageBox.Ok)
        return msg.exec_()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ui = CreatePlaylistForm()
    ui.show()
    sys.exit(app.exec_())

