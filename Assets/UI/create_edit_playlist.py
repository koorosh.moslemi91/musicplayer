# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'create_edit_playlist.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(457, 528)
        self.splitter = QtWidgets.QSplitter(Form)
        self.splitter.setGeometry(QtCore.QRect(10, 10, 431, 31))
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.playlistNameLabel = QtWidgets.QLabel(self.splitter)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.playlistNameLabel.setFont(font)
        self.playlistNameLabel.setObjectName("playlistNameLabel")
        self.playListNameLineEdit = QtWidgets.QLineEdit(self.splitter)
        self.playListNameLineEdit.setObjectName("playListNameLineEdit")
        self.splitter_2 = QtWidgets.QSplitter(Form)
        self.splitter_2.setGeometry(QtCore.QRect(10, 40, 441, 481))
        self.splitter_2.setOrientation(QtCore.Qt.Vertical)
        self.splitter_2.setObjectName("splitter_2")
        self.songsLabel = QtWidgets.QLabel(self.splitter_2)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.songsLabel.setFont(font)
        self.songsLabel.setObjectName("songsLabel")
        self.songsTreeView = QtWidgets.QTreeView(self.splitter_2)
        self.songsTreeView.setObjectName("songsTreeView")
        self.confirmButton = QtWidgets.QPushButton(self.splitter_2)
        self.confirmButton.setObjectName("confirmButton")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Create Playlist"))
        self.playlistNameLabel.setText(_translate("Form", "Playlist Name:"))
        self.songsLabel.setText(_translate("Form", "Songs:"))
        self.confirmButton.setText(_translate("Form", "Create"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
