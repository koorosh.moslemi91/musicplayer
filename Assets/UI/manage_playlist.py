# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'manage_playlist.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(657, 417)
        self.playlistsTreeView = QtWidgets.QTreeView(Form)
        self.playlistsTreeView.setGeometry(QtCore.QRect(10, 10, 641, 301))
        self.playlistsTreeView.setObjectName("playlistsTreeView")
        self.createButton = QtWidgets.QPushButton(Form)
        self.createButton.setGeometry(QtCore.QRect(10, 370, 641, 41))
        self.createButton.setObjectName("createButton")
        self.splitter = QtWidgets.QSplitter(Form)
        self.splitter.setGeometry(QtCore.QRect(10, 320, 641, 41))
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.removeButton = QtWidgets.QPushButton(self.splitter)
        self.removeButton.setObjectName("removeButton")
        self.editButton = QtWidgets.QPushButton(self.splitter)
        self.editButton.setObjectName("editButton")
        self.playButton = QtWidgets.QPushButton(self.splitter)
        self.playButton.setObjectName("playButton")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Manage Playlists"))
        self.createButton.setText(_translate("Form", "Create Playlist"))
        self.removeButton.setText(_translate("Form", "Remove Playlist"))
        self.editButton.setText(_translate("Form", "Edit Playlist"))
        self.playButton.setText(_translate("Form", "Play Playlist"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
