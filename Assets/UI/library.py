# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'library.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_LibraryForm(object):
    def setupUi(self, LibraryForm):
        LibraryForm.setObjectName("LibraryForm")
        LibraryForm.resize(710, 442)
        self.libraryView = QtWidgets.QTreeView(LibraryForm)
        self.libraryView.setGeometry(QtCore.QRect(10, 40, 691, 311))
        self.libraryView.setObjectName("libraryView")
        self.libraryLabel = QtWidgets.QLabel(LibraryForm)
        self.libraryLabel.setGeometry(QtCore.QRect(10, 10, 61, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.libraryLabel.setFont(font)
        self.libraryLabel.setObjectName("libraryLabel")
        self.groupBox = QtWidgets.QGroupBox(LibraryForm)
        self.groupBox.setGeometry(QtCore.QRect(270, 360, 431, 71))
        self.groupBox.setObjectName("groupBox")
        self.splitter = QtWidgets.QSplitter(self.groupBox)
        self.splitter.setGeometry(QtCore.QRect(10, 20, 411, 41))
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.gbArtistButton = QtWidgets.QPushButton(self.splitter)
        self.gbArtistButton.setObjectName("gbArtistButton")
        self.gbGenreButton = QtWidgets.QPushButton(self.splitter)
        self.gbGenreButton.setObjectName("gbGenreButton")
        self.gbAlbumButton = QtWidgets.QPushButton(self.splitter)
        self.gbAlbumButton.setObjectName("gbAlbumButton")
        self.gbNoneButton = QtWidgets.QPushButton(self.splitter)
        self.gbNoneButton.setObjectName("gbNoneButton")
        self.splitter_2 = QtWidgets.QSplitter(LibraryForm)
        self.splitter_2.setGeometry(QtCore.QRect(10, 380, 251, 41))
        self.splitter_2.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_2.setObjectName("splitter_2")
        self.addButton = QtWidgets.QPushButton(self.splitter_2)
        self.addButton.setObjectName("addButton")
        self.removeButton = QtWidgets.QPushButton(self.splitter_2)
        self.removeButton.setObjectName("removeButton")

        self.retranslateUi(LibraryForm)
        QtCore.QMetaObject.connectSlotsByName(LibraryForm)

    def retranslateUi(self, LibraryForm):
        _translate = QtCore.QCoreApplication.translate
        LibraryForm.setWindowTitle(_translate("LibraryForm", "Library"))
        self.libraryLabel.setText(_translate("LibraryForm", "Library:"))
        self.groupBox.setTitle(_translate("LibraryForm", "Group By:"))
        self.gbArtistButton.setText(_translate("LibraryForm", "Artist"))
        self.gbGenreButton.setText(_translate("LibraryForm", "Genre"))
        self.gbAlbumButton.setText(_translate("LibraryForm", "Album"))
        self.gbNoneButton.setText(_translate("LibraryForm", "None"))
        self.addButton.setText(_translate("LibraryForm", "Add"))
        self.removeButton.setText(_translate("LibraryForm", "Remove"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    LibraryForm = QtWidgets.QWidget()
    ui = Ui_LibraryForm()
    ui.setupUi(LibraryForm)
    LibraryForm.show()
    sys.exit(app.exec_())
