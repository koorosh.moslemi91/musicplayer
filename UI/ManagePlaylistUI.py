from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QAbstractItemView

#def setupUi(self):
#    self.setObjectName("Form")
#    self.resize(657, 377)
#    self.playlistsTreeView = QtWidgets.QTreeView(self)
#    self.playlistsTreeView.setGeometry(QtCore.QRect(10, 10, 641, 301))
#    self.playlistsTreeView.setSelectionMode(QAbstractItemView.SingleSelection)
#    self.playlistsTreeView.setEditTriggers(QAbstractItemView.NoEditTriggers)
#    self.playlistsTreeView.setObjectName("playlistsTreeView")
#    self.splitter = QtWidgets.QSplitter(self)
#    self.splitter.setGeometry(QtCore.QRect(10, 320, 641, 51))
#    self.splitter.setOrientation(QtCore.Qt.Horizontal)
#    self.splitter.setObjectName("splitter")
#    self.removeButton = QtWidgets.QPushButton(self.splitter)
#    self.removeButton.setObjectName("removeButton")
#    self.editButton = QtWidgets.QPushButton(self.splitter)
#    self.editButton.setObjectName("editButton")

#    retranslateUi(self)
#    QtCore.QMetaObject.connectSlotsByName(self)

#def retranslateUi(self):
#    _translate = QtCore.QCoreApplication.translate
#    self.setWindowTitle(_translate("Form", "Manage Playlists"))
#    self.removeButton.setText(_translate("Form", "Remove Playlist"))
#    self.editButton.setText(_translate("Form", "Edit Playlist"))


#def setupUi(self):
#    self.setObjectName("Form")
#    self.resize(657, 424)
#    self.playlistsTreeView = QtWidgets.QTreeView(self)
#    self.playlistsTreeView.setGeometry(QtCore.QRect(10, 10, 641, 301))
#    self.playlistsTreeView.setSelectionMode(QAbstractItemView.SingleSelection)
#    self.playlistsTreeView.setEditTriggers(QAbstractItemView.NoEditTriggers)
#    self.playlistsTreeView.setObjectName("playlistsTreeView")
#    self.splitter = QtWidgets.QSplitter(self)
#    self.splitter.setGeometry(QtCore.QRect(10, 320, 641, 51))
#    self.splitter.setOrientation(QtCore.Qt.Horizontal)
#    self.splitter.setObjectName("splitter")
#    self.removeButton = QtWidgets.QPushButton(self.splitter)
#    self.removeButton.setObjectName("removeButton")
#    self.editButton = QtWidgets.QPushButton(self.splitter)
#    self.editButton.setObjectName("editButton")
#    self.createButton = QtWidgets.QPushButton(self)
#    self.createButton.setGeometry(QtCore.QRect(10, 370, 641, 51))
#    self.createButton.setObjectName("createButton")

#    retranslateUi(self)
#    QtCore.QMetaObject.connectSlotsByName(self)

#def retranslateUi(self):
#    _translate = QtCore.QCoreApplication.translate
#    self.setWindowTitle(_translate("Form", "Manage Playlists"))
#    self.removeButton.setText(_translate("Form", "Remove Playlist"))
#    self.editButton.setText(_translate("Form", "Edit Playlist"))
#    self.createButton.setText(_translate("Form", "Create Playlist"))


def setupUi(self):
    self.setObjectName("Form")
    self.resize(657, 417)
    self.playlistsTreeView = QtWidgets.QTreeView(self)
    self.playlistsTreeView.setGeometry(QtCore.QRect(10, 10, 641, 301))
    self.playlistsTreeView.setObjectName("playlistsTreeView")
    self.createButton = QtWidgets.QPushButton(self)
    self.createButton.setGeometry(QtCore.QRect(10, 370, 641, 41))
    self.createButton.setObjectName("createButton")
    self.splitter = QtWidgets.QSplitter(self)
    self.splitter.setGeometry(QtCore.QRect(10, 320, 641, 41))
    self.splitter.setOrientation(QtCore.Qt.Horizontal)
    self.splitter.setObjectName("splitter")
    self.removeButton = QtWidgets.QPushButton(self.splitter)
    self.removeButton.setObjectName("removeButton")
    self.editButton = QtWidgets.QPushButton(self.splitter)
    self.editButton.setObjectName("editButton")
    self.playButton = QtWidgets.QPushButton(self.splitter)
    self.playButton.setObjectName("playButton")

    retranslateUi(self)
    QtCore.QMetaObject.connectSlotsByName(self)

def retranslateUi(self):
    _translate = QtCore.QCoreApplication.translate
    self.setWindowTitle(_translate("Form", "Manage Playlists"))
    self.createButton.setText(_translate("Form", "Create Playlist"))
    self.removeButton.setText(_translate("Form", "Remove Playlist"))
    self.editButton.setText(_translate("Form", "Edit Playlist"))
    self.playButton.setText(_translate("Form", "Play Playlist"))