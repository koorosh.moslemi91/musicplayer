from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QAbstractItemView



def setupUi(self):
        self.setObjectName("Form")
        self.resize(457, 528)
        self.splitter = QtWidgets.QSplitter(self)
        self.splitter.setGeometry(QtCore.QRect(10, 10, 431, 31))
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.playlistNameLabel = QtWidgets.QLabel(self.splitter)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.playlistNameLabel.setFont(font)
        self.playlistNameLabel.setObjectName("playlistNameLabel")
        self.playListNameLineEdit = QtWidgets.QLineEdit(self.splitter)
        self.playListNameLineEdit.setObjectName("playListNameLineEdit")
        self.splitter_2 = QtWidgets.QSplitter(self)
        self.splitter_2.setGeometry(QtCore.QRect(10, 40, 441, 481))
        self.splitter_2.setOrientation(QtCore.Qt.Vertical)
        self.splitter_2.setObjectName("splitter_2")
        self.songsLabel = QtWidgets.QLabel(self.splitter_2)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.songsLabel.setFont(font)
        self.songsLabel.setObjectName("songsLabel")
        #self.songsTreeView = QtWidgets.QTreeView(self.splitter_2)
        self.songsTreeView = QtWidgets.QTreeWidget(self.splitter_2)
        self.songsTreeView.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.songsTreeView.setObjectName("songsTreeView")
        self.confirmButton = QtWidgets.QPushButton(self.splitter_2)
        self.confirmButton.setObjectName("confirmButton")

        retranslateUi(self)
        QtCore.QMetaObject.connectSlotsByName(self)

def retranslateUi(self):
    _translate = QtCore.QCoreApplication.translate
    self.setWindowTitle(_translate("Form", "Create Playlist"))
    self.playlistNameLabel.setText(_translate("Form", "Playlist Name:"))
    self.songsLabel.setText(_translate("Form", "Songs:"))
    self.confirmButton.setText(_translate("Form", "Create Playlist"))
